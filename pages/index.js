import React, { useState, useEffect } from 'react';
import styles from '../styles/Home.module.css';

export default function Home() {
  const [navOpen, setNavOpen] = useState(false);
  const [scrolled, setScrolled] = React.useState(false);

  // Responsive Navbar
  const handleNavOpen = () => {
    setNavOpen(!navOpen);
  };

  // Sticky Navigation
  const handleScroll = () => {
    const offset = window.scrollY;

    if (offset > 200) {
      setScrolled(true);
    } else {
      setScrolled(false);
    }
  };
  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
  }, []);

  return (
    <div className={`${scrolled ? 'sticky' : ''}`}>
      <header className={`${navOpen ? 'nav-open' : ''} header`}>
        <a className={styles.title} href="#">
          Hunger Apps
        </a>

        <nav className="main-nav">
          <ul className="main-nav-list">
            <li>
              <a className="main-nav-link" href="#">
                Home
              </a>
            </li>
            <li>
              <a className="main-nav-link" href="#">
                Favorite
              </a>
            </li>
            <li>
              <a className="main-nav-link" href="#">
                About Us
              </a>
            </li>
          </ul>
        </nav>

        <button className="btn-mobile-nav" onClick={handleNavOpen}>
          <img
            className="icon-mobile-nav"
            name="menu-outline"
            src="/menu-outline.svg"
          ></img>
          <img
            className="icon-mobile-nav"
            name="close-outline"
            src="/close-outline.svg"
          ></img>
        </button>
      </header>

      <main className={styles.main}>
        <section className="section-hero">
          <div className="hero">
            <div className="hero-text-box">
              <h1>A healthy restaurant found nearby you, every single day</h1>
              <p className="hero-description">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Magnam
                amet error voluptatibus fuga in distinctio aspernatur, ad,
                quibusdam voluptate, cumque nostrum sed delectus officia
                veritatis.
              </p>
              <a href="#" className="btn btn--full margin-right-sm">
                Start eating well
              </a>
              <a href="#" className="btn btn--outline">
                Learn more &darr;
              </a>
              <div className="rated-restaurant">
                <div className="rated-imgs">
                  <img src="/customers/customer-1.jpg" alt="Customer photo" />
                  <img src="/customers/customer-2.jpg" alt="Customer photo" />
                  <img src="/customers/customer-3.jpg" alt="Customer photo" />
                  <img src="/customers/customer-4.jpg" alt="Customer photo" />
                  <img src="/customers/customer-5.jpg" alt="Customer photo" />
                  <img src="/customers/customer-6.jpg" alt="Customer photo" />
                </div>
                <p className="rated-text">
                  <span>250,000+</span> restaurant rated!
                </p>
              </div>
            </div>
            <div className="hero-img-box">
              <img src="/hero.png" className="hero-img" />
            </div>
          </div>
        </section>

        <section className="section-restaurant" id="restaurant">
          <div className="container center-text">
            <h1 className="heading-restaurant">Explore Restaurant</h1>
          </div>

          <div className="container grid grid--3-cols margin-bottom-md">
            <div className="restaurant">
              <img
                src="/restaurant/restaurant-1.jpg"
                className="restaurant-img"
              />
              <div className="restaurant-city">
                <p>Jakarta</p>
              </div>
              <div className="restaurant-content">
                <div className="restaurant-rate">
                  <img name="star-fill" src="/star-fill.svg"></img>
                  <span className="rate">Rating : 4.5</span>
                </div>
                <p className="restaurant-title">Japanese Gyozas</p>
                <p className="restaurant-description">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Itaque, optio!
                </p>
              </div>
            </div>

            <div className="restaurant">
              <img
                src="/restaurant/restaurant-2.jpg"
                className="restaurant-img"
              />
              <div className="restaurant-city">
                <p>Bandung</p>
              </div>
              <div className="restaurant-content">
                <div className="restaurant-rate">
                  <img name="star-fill" src="/star-fill.svg"></img>
                  <span className="rate">Rating : 4.5</span>
                </div>
                <p className="restaurant-title">Korean BBQ</p>
                <p className="restaurant-description">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Itaque, optio!
                </p>
              </div>
            </div>

            <div className="restaurant">
              <img
                src="/restaurant/restaurant-3.jpeg"
                className="restaurant-img"
              />
              <div className="restaurant-city">
                <p>Makassar</p>
              </div>
              <div className="restaurant-content">
                <div className="restaurant-rate">
                  <img name="star-fill" src="/star-fill.svg"></img>
                  <span className="rate">Rating : 4.5</span>
                </div>
                <p className="restaurant-title">Padang Payakumbuah</p>
                <p className="restaurant-description">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Itaque, optio!
                </p>
              </div>
            </div>
          </div>
        </section>
      </main>

      <footer className={styles.footer}>
        <a href="#">Copyright &copy; 2020 &mdash; Hunger Apps</a>
      </footer>
    </div>
  );
}
